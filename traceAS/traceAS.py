import argparse
import socket
from urllib.request import urlopen
from json import loads


def main():
    parser = init_arguments()
    try:
        dest = socket.gethostbyname(parser.destination)
    except:
        print("Something went wrong. Check out your internet connection and "
              "correctness of the query.")
        return
    for line in traceroute(dest, parser.max_ttl):
        print(line)


def init_arguments():
    parser = argparse.ArgumentParser(prog='Simple traceroute utility',
                                     usage='traceAS.py [-h hops] '
                                           'destination')
    parser.add_argument('destination', type=str,
                        help='domain name or ip address',
                        default=None)
    parser.add_argument('-t', '--max-ttl', type=int,
                        help='maximum ttl', default=20)
    return parser.parse_args()


def traceroute(destination, hops):
    sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
    sock.settimeout(2)
    current_address = ""
    ttl = 1
    while ttl != hops and current_address != destination:
        sock.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
        sock.sendto(b'\x08\x00\x0b\x27\xeb\xd8\x01\x00', (destination, 1))
        try:
            _, addr = sock.recvfrom(1024)
            current_address = addr[0]
            info = ''.join([c for c in get_ip_info(current_address)])
            yield 'TTL: {}, router IP: {} {}'.format(ttl, current_address,
                                                     info)
            ttl += 1
        except socket.timeout:
            ttl += 1
            continue
    sock.close()


def get_ip_info(ip):
    info = loads(urlopen('http://ipinfo.io/{}/json'.format(ip)).read())
    for key in info:
        if key == 'ip':
            continue
        if key == 'bogon':
            yield '\n\tip is local'
            continue
        if info[key] == '':
            info[key] = 'unknown'
        yield '\n\t{}: {} '.format(key, info[key])


if __name__ == '__main__':
    main()
